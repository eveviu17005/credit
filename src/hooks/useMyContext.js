import { useContext } from 'react'
import { DataContext } from '../context/ContextProvider'

export default function useMyContext() {
  return useContext(DataContext)
}
