import React from 'react'

import { Pagination } from 'swiper/modules';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/pagination';
import Slider from './Slider';

export default function Banner() {
    return (

        <div className='mt-[12px] md:mt-[32px] h-auto lg:h-[400px]'>
            <Swiper pagination={true} modules={[Pagination]} className="mySwiper">
                <SwiperSlide>
                    <Slider />
                </SwiperSlide>
                <SwiperSlide>
                    <Slider />
                </SwiperSlide>
                <SwiperSlide>
                    <Slider />
                </SwiperSlide>
            </Swiper>

        </div>
    )
}
