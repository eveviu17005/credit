/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        green: "#3CB371",
        boldBlack: "#1E1E1E",
        normalBlack: "#333333",
      },
    },
  },
  plugins: [],
};
